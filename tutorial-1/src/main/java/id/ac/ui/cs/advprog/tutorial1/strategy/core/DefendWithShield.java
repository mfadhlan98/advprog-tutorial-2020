package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    private String defenceType = "Shield";

    @Override
    public String defend() {
        return "Defend with " + defenceType + "!!";
    }

    @Override
    public String getType() {
        return defenceType;
    }
}
