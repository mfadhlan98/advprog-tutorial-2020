package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    private String alias = "Mystic";

    public MysticAdventurer(){
        setAttackBehavior(new AttackWithMagic());
        setDefenseBehavior(new DefendWithShield());
    }

    @Override
    public String getAlias() {
        return alias;
    }
}
