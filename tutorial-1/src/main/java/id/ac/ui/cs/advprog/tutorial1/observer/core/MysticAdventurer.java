package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                guild.add(this);
        }

        @Override
        public void update() {
                System.out.println("Mystic : " + guild.getQuestType());
                if (guild.getQuestType().equals("D") || guild.getQuestType().equals("E")){
                        this.getQuests().add(guild.getQuest());
                }
        }

}
