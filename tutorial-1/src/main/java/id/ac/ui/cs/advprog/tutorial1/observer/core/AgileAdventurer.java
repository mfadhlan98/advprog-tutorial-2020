package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild;
                guild.add(this);
        }

        @Override
        public void update() {
                System.out.println("Agile : "+guild.getQuestType());
                if (guild.getQuestType().equals("D") || guild.getQuestType().equals("R")){
                        this.getQuests().add(guild.getQuest());
                }
        }

}
