package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    private String defenceType = "Barrier";

    @Override
    public String defend() {
        return "Defend with " + defenceType + "!!";
    }

    @Override
    public String getType() {
        return defenceType;
    }
}
