package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public String countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            return powerClassiefier(rawAge*2000);
        } else if (rawAge <50) {
            return powerClassiefier(rawAge*2250);
        } else {
            return powerClassiefier(rawAge*5000);
        }
    }

    public String powerClassiefier (int power) {
        if (power<20001){
            return power + ", you are in C Class";
        } else if (power>100000) {
            return power + ", you are in A Class";
        } else {
            return power + ", you are in B Class";
        }
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
